const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    title: 'LiveCode.NYC',
    description: '',

    dest: './public/',

    themeConfig: {
        repo: 'https://gitlab.com/livecodenyc/livecodenyc.gitlab.io',
        mailingList: 'https://groups.google.com/forum/#!forum/livecodenyc',
        social: [
            { url: 'https://twitter.com/livecodenyc', title: 'Twitter' },
            { url: 'https://instagram.com/livecodenyc', title: 'Instagram' },
            { url: 'https://www.facebook.com/livecodenyc', title: 'Facebook'},
            { url: 'https://gitlab.com/livecodenyc', title: 'GitLab' }
        ],
        footerLinks: [
            { path: '/how-to-edit', title: 'How to Submit Edits' },
            { path: '/design', title: 'Design Assets' },
            { path: '/coc', title: 'Code of Conduct' }
        ]
    },

    markdown: {
        anchor: {
            permalink: true,
            permalinkBefore: false,
            permalinkSymbol: ''
        }
    },
 
    configureWebpack: (config, isServer) => {
        return {
            plugins: [
                new CopyWebpackPlugin([
                    { from: 'image', to: 'image' }
                ])
            ]
        }
    }
}