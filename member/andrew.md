---
name: Andrew Cotter
image: https://pbs.twimg.com/media/DipdnCsU8AAkJpJ.jpg:large
links:
    website: http://thatcotter.github.io
---

Makes interactive installations, video games, and teaches creative coding to grad students. 
