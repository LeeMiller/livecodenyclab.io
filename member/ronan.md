---
name: Ronan Rice
image: http://ronanrice.com/img/livecode.jpg
links:
    website: http://ronanrice.com
---

Harlem-based semiconductor engineer. Lives for music, art and racing.
