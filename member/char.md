---
name: Char Stiles
image: http://charstiles.com/wp-content/uploads/2019/04/20170326-DSC02147-768x512.jpg
links:
    website: http://charstiles.com
    twitter: https://twitter.com/CharStiles
    instagram: https://www.instagram.com/charstiles/
---

Computational artist, livecoding shaders & giving workshops. Thinking about email.