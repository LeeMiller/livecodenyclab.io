---
name: Jessica Garson
image: https://pbs.twimg.com/profile_images/1100779457906966528/-X_SEREP_400x400.jpg
links:
    website: http://www.jessicagarson.com/
---
Brooklyn based artist, musician, educator and programmer. Known sometimes as Messica Arson.
