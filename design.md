---
title: Design Assets
---
# Design Assets

## Logo

<img src="/media/logos/LiveCodeNYC_logo_white.svg" height="100">

[Download SVG](/media/logos/LiveCodeNYC_logo_white.svg),
[Download AI](/media/logos/LiveCodeNYC_logo_white.ai),
[Download EPS](/media/logos/LiveCodeNYC_logo_white.eps)

<img src="/media/logos/LiveCodeNYC_logo_black.svg" height="100">

[Download SVG](/media/logos/LiveCodeNYC_logo_black.svg),
[Download AI](/media/logos/LiveCodeNYC_logo_black.ai),
[Download EPS](/media/logos/LiveCodeNYC_logo_black.eps)

## Colors
<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0;width:200px;border-radius:30px;font-size:0.8em;background-color:rgba(0,0,51,1.0);color:#ffffff;">
Deep Blue
<br>
<span class="mono">RGB(0,0,51)</span>
<br>
<span class="mono">#000033</span>
</div>
<div style="padding:20px;width:200px;display:inline-block;margin:1rem 1rem 1rem 0;border-radius:30px;font-size:0.8em;background-color:rgba(51,255,255,1.0);color:#000000;">
Electric Blue
<br>
<span class="mono">RGB(51,255,255)</span>
<br>
<span class="mono">#33FFFF</span>
</div>
<div style="padding:20px;display:inline-block;margin:1rem 1rem 1rem 0;
font-size:0.8em;width:200px;border-radius:30px;background-color:rgba(153,255,0,1.0);color:#000000;">
Electric Green
<br>
<span class="mono">RGB(153,255,0)</span>
<br>
<span class="mono">#99FF00</span>
</div>
