---
title: How to Edit
---

# How to Submit Edits

The source code for this website can be found on [GitLab](https://gitlab.com/livecodenyc/livecodenyc.gitlab.io). LiveCode.NYC members are invited to submit Pull Requests to keep information up to date.